#!/usr/bin/env python3

"""
"template engine" für meine gemini seiten lol

warum ne template engine für so ein einfaches format?

zb weil es damit einfacher ist ascii art zu generieren oder
daten in posts einzufügen
"""

import json
import os
import re
import time
import datetime
import subprocess

import click
from pyfiglet import Figlet


def fatal(message):
    click.secho(f'ERROR: {message}', fg='red')
    exit(-1)


def current_time(template_dir, args, **kwargs):
    """
    insert current time of compilation
    """
    return str(datetime.datetime.now())


def artwork(template_dir, args, **kwargs):
    """
    insert ascii art
    """
    f = Figlet()
    text = ' '.join(args)
    return f'```{text} ASCII art\n{f.renderText(text)}\n```'


def include(template_dir, args, **kwargs):
    """
    include a template file
    
    warning: weird things will happen if there are cyclic dependencies
    """
    template = ' '.join(args)
    if not template.endswith('.gmi'):
        template += '.gmi'
    return render(template_dir, template)


def postlist(template_dir, args, **kwargs):
    """
    render a list of all posts in specified posts directory
    """
    posts_dir = kwargs.pop('posts', './posts')
    posts = os.listdir(posts_dir)
    
    def post_get_name(post):
        path = os.path.join(posts_dir, post)
        with open(path, 'r') as f:
            text = f.read()
        lines = text.splitlines()
        lines = [line.strip() for line in lines if line.strip().startswith('#')]
        if lines:
            return ' '.join(lines[0].split(' ')[1:])
        return ''
    
    posts = [f'=> posts/{post} {post_get_name(post)}' for post in posts]
    return '\n'.join(posts)


def lastupdate(template_dir, args, **kwargs):
    filename = os.path.join(template_dir, kwargs['template'])
    return str(datetime.datetime.fromtimestamp(os.path.getmtime(filename)))


def back(template_dir, args, **kwargs):
    return '=> / back\n'


keywords = {
    'now': current_time,
    'artwork': artwork,
    'include': include,
    'postlist': postlist,
    'last_update': lastupdate,
    'back': back
}


def parse_expression(template_dir, expression, **kwargs):
    if not expression:
        return ''
    tokens = expression.split()
    command = tokens[0]
    args = []
    if len(tokens) > 1:
        args = tokens[1:]
    if command in keywords:
        return keywords[command](template_dir, args, **kwargs)
    return ''


def template_text(template_dir, text, **kwargs):
    expressions = re.findall('{.*?}', text)
    updates = []
    offset = 0
    for expression in expressions:
        start = text.index(expression)
        end = start + len(expression)
        start += offset
        end += offset
        new = parse_expression(template_dir, expression[1:-1].strip(), **kwargs)
        offset += len(new) - len(expression)
        updates.append((start, end, new, expression))
    for update in updates:
        start, end, new, old = update
        text = text[:start] + new + text[end:]
    # allow escaping curly brackets
    text = text.replace('\{', '{')
    text = text.replace('\}', '}')
    return text


def render(template_dir, template, **kwargs):
    with open(os.path.join(template_dir, template), 'r') as f:
        text = f.read()
    return template_text(template_dir, text, **{**kwargs, 'template': template})


def render_to_file(template_dir, output_dir, **kwargs):
    templates = os.listdir(template_dir)
    os.makedirs(output_dir, exist_ok=True)
    for template in templates:
        templated = render(template_dir, template, **kwargs)
        with open(os.path.join(output_dir, template), 'w') as f:
            f.write(templated)


def upload_scp(output):
    with open('config.json', 'r') as f:
        config = json.load(f)
    ssh = config['ssh']
    user = ssh.get('user')
    host = ssh.get('host')
    directory = ssh.get('directory')

    cmd = [
        'scp',
        '-r',
        f'{output}/.',
        f'{user}@{host}:{directory}/'
    ]
    subprocess.call(cmd)


@click.command()
@click.option('-t', '--templates', default='./templates')
@click.option('-o', '--output', default='./output')
@click.option('-p', '--posts', default='./posts')
def fmain(templates, output, posts):
    if not os.path.isdir(templates):
        fatal(f'Not a template directory: "{templates}"')
    if 'index.gmi' not in os.listdir(templates):
        fatal(f'No index.gmi template found')
    kwargs = {
        'templates': templates,
        'output': output,
        'posts': posts
    }
    render_to_file(templates, output, **kwargs)
    render_to_file(posts, os.path.join(output, 'posts'), **kwargs)
    upload_scp(output)


if __name__ == '__main__':
    fmain()
